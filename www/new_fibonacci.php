<?php

// TODO: make this a recursive function that returns
//       an array of the given length containing the
//       members of the fibonacci sequence
//
// you may reuse the code in old_fibonacci.php as you wish
// you may not add any other functions to solve this problem
// you must call make_Fibonacci_Sequence recursively

function make_Fibonacci_Sequence($length, $list=[]){
$last_number = $list[ sizeof($list)-1];
if(count($list) === 0){
 $answer = 0;   
}
$second_last_number = $list[ sizeof($list)-2];
if(count($list) === 1){
 $answer = 1;   
}
if(sizeof($list) > 1){
 $answer = $last_number + $second_last_number;
}
$list[] = $answer;
if(sizeof($list) < $length){
 $list = make_Fibonacci_Sequence($length, $list);   
}
return $list;
}


?>
