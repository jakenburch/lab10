<?php

/*
  uncomment the new fibinacci include
  and comment out the old fibinacci include
  to see your work. old fibonacci is there
  so you may refactor that code
*/

// include 'old_fibonacci.php';   // TODO: comment out this line
include 'new_fibonacci.php'; // TODO: uncomment this line

$fibonacci = make_Fibonacci_Sequence(10);

?><!DOCTYPE html>
<html>
  <body>
    <h1>Fibonacci!</h1>
    <?php foreach($fibonacci as $number){ ?>
      <li><?= $number ?></li>
    <?php } ?>
  </body>
</html>
