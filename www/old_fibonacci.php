<?php

function make_Fibonacci_Sequence($length){
  $fibonacci = [];               
  for($i=0; $i<$length; $i++){  
    $fibonacci[] = find_Next_In_Sequence($fibonacci);
  }
  return $fibonacci;           
}

function find_Next_In_Sequence($list){
  $last_number = $list[ sizeof($list)-1];
  if(count($list) === 0){
    $answer = 0;
  }

  $second_last_number = $list[ sizeof($list)-2];

  if(count($list) === 1){
    $answer = 1;
  }

  if(sizeof($list) > 1){
    $answer = $last_number + $second_last_number;
  }

  return $answer;
}

?>
