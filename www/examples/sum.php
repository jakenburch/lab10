<?php 
function sum($ray, $value=0){
  $result = $value;   // pre-set result to the value 
  if(!empty($ray)){
    $num = array_shift($ray);
    $value += $num; // same as $value = $value + $num;
    $result = sum($ray, $value);
  }
  return $result;
}

echo sum([5, 10, 15, 20]);

?>
