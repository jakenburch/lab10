<?php
  function infinity($saying){
    echo $saying;
    infinity($saying);
  }

  /******WARNING***********
   *
   * running the following code
   * will definitely cause an infinite loop
   * it will resolve itself eventually
   * but you will have to wait

  infinity("and beyond…");

   * you have been warned
   ************************/
?>
